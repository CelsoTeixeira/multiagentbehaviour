using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World : MonoBehaviour
{
    public Transform AgentPrefab;
    public int NumberAgents;

    public List<Agent> Agents; 

    void Start()
    {
        Agents = new List<Agent>();

        Spawn(AgentPrefab, NumberAgents);

        Agents.AddRange(FindObjectsOfType<Agent>());
    }

    void Update()
    {
        

    }

    void Spawn(Transform prefab, int n)
    {
        for (int i = 0; i < n; i++)
        {
            var obj = Instantiate(prefab, 
                                new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10)),
                                Quaternion.identity);
        }
    }

    public List<Agent> GetNeighbors(Agent agent, float radius)
    {
        List<Agent> r = new List<Agent>();

        foreach (var otherAgent in Agents)
        {
            if (otherAgent == agent)
                continue;

            if (Vector3.Distance(agent._position, otherAgent._position) <= radius)
                r.Add(otherAgent);
        }

        return r;

    } 
}