using UnityEngine;
using System.Collections;

public class AgentConfig : MonoBehaviour
{
    public float Rc;        //Cohesion radius
    public float Rs;
    public float Ra;

    public float Kc;
    public float Ks;
    public float Ka;

    public float MaxV;
    public float MaxA;

    public float MaxFieldOfViewAngle = 180;
}