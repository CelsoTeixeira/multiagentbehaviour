using UnityEngine;
using System.Collections;

public class Agent : MonoBehaviour
{
    public Vector3 _position;
    public Vector3 _velocity;
    public Vector3 _acceleration;
    public World _world;

    public AgentConfig _config;

    void Awake()
    {
        _world = FindObjectOfType<World>();
        _config = FindObjectOfType<AgentConfig>();
    }

    void Start()
    {
        _position = transform.position;
    }

    void Update()
    {
        float t = Time.deltaTime;

        _acceleration = Combine();
        _acceleration = Vector3.ClampMagnitude(_acceleration, _config.MaxA);

        _velocity = _velocity + _acceleration * t;
        _velocity = Vector3.ClampMagnitude(_velocity, _config.MaxV);

        _position = _position + _velocity * t;

        transform.position = _position;


    }

    Vector3 Cohesion()
    {
        Vector3 r = new Vector3();

        var neighs = _world.GetNeighbors(this, _config.Rc);

        int countAgents = 0;

        if (neighs.Count == 0)
            return r;

        foreach (var agent in neighs)
        {
            if (IsInFieldOfView(agent._position))
            {
                r += agent._position;
                countAgents ++;
            }
        }

        if (countAgents == 0)
            return r;

        r /= countAgents;

        r = r - this._position;

        Vector3.Normalize(r);

        return r;
    }

    Vector3 Separation()
    {
        Vector3 r = new Vector3();

        var agents = _world.GetNeighbors(this, _config.Rs);

        if (agents.Count == 0)
            return r;

        foreach (var agent in agents)
        {
            if (IsInFieldOfView(agent._position))
            {
                Vector3 towardsMe = this._position - agent._position;

                if (towardsMe.magnitude > 0)
                {
                    r += towardsMe.normalized / towardsMe.magnitude;
                }
            }
        }

        return r.normalized;

 ;   }

    Vector3 Alignment()
    {
        Vector3 r = new Vector3();

        var agents = _world.GetNeighbors(this, _config.Ra);

        if (agents.Count == 0)
            return r;

        foreach (var agent in agents)
        {
            if (IsInFieldOfView(agent._position))
                r += agent._velocity;
        }

        return Vector3.Normalize(r);

    }

    Vector3 Combine()
    {
        Vector3 r = _config.Kc * Cohesion() + _config.Ks * Separation() + _config.Ka * Alignment();
        return r;
    }

    bool IsInFieldOfView(Vector3 stuff)
    {
        return Vector3.Angle(this._velocity, stuff - this._position) <= _config.MaxFieldOfViewAngle;
    }
}